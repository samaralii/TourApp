package com.ss.locationapp.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.ss.locationapp.Listeners;
import com.ss.locationapp.POJOS.LocPOJO;
import com.ss.locationapp.POJOS.POJO;
import com.ss.locationapp.R;
import com.ss.locationapp.Utilz;
import com.ss.locationapp.backgroudServices.BackgroundTask;
import com.ss.locationapp.ui.adapters.NearbyLocationAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AndroidLocationActivity extends BaseActivity implements LocationListener {

    public static final String DATA = "data";
    private static final String TAG = "BroadcastTest";
    List<POJO> locationList;
    //    private TextView latitude, longitude;
    @BindView(R.id.activity_main_new_list)
    RecyclerView recyclerView;
    ArrayList<com.ss.locationapp.POJOS.Location> data;
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double fusedLatitude = 0.0;
    private double fusedLongitude = 0.0;
    private double lat = 0.0;
    private double lon = 0.0;
    private LocPOJO locPOJO;
    private Intent _intent;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateUI(intent);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_new);
        ButterKnife.bind(this);
        startBackgroundTask();

        _intent = new Intent(this, BackgroundTask.class);
    }

    private void updateUI(Intent intent) {

        if (intent.getStringExtra("error") != null) {
            Utilz.tmsg(this, "Error");
            return;
        }


        data = intent.getParcelableArrayListExtra("counter");
//        Utilz.tMsg(this, counter);


        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        NearbyLocationAdapter adapter = new NearbyLocationAdapter(data,
                new Listeners.LocationListListener() {
                    @Override
                    public void onClick(com.ss.locationapp.POJOS.Location location) {
                        Intent i = new Intent(AndroidLocationActivity.this, DetailActivity.class);

                        //Setting Current Location
                        LocPOJO locPOJO = new LocPOJO();
                        locPOJO.setCurrent(new LatLng(lat, lon));
                        location.setLocPOJO(locPOJO);

                        i.putExtra(DATA, location);
                        startActivity(i);
                    }
                });

        recyclerView.setAdapter(adapter);


    }

    public void startBackgroundTask() {
        Intent i = new Intent(this, BackgroundTask.class);
        i.putExtra(BackgroundTask.DATA, "Hello");
        startService(i);
    }


    @Override
    public void onResume() {
        super.onResume();
        startService(_intent);
        registerReceiver(broadcastReceiver, new IntentFilter(BackgroundTask.BROADCAST_ACTION));
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
        stopService(_intent);
    }


    private void initializeViews() {


        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        NearbyLocationAdapter adapter = new NearbyLocationAdapter(new ArrayList<com.ss.locationapp.POJOS.Location>(),
                new Listeners.LocationListListener() {
                    @Override
                    public void onClick(com.ss.locationapp.POJOS.Location location) {
                        Intent i = new Intent(AndroidLocationActivity.this, DetailActivity.class);

                        //Setting Current Location
                        LocPOJO locPOJO = new LocPOJO();
                        locPOJO.setCurrent(new LatLng(lat, lon));
                        location.setLocPOJO(locPOJO);

                        i.putExtra(DATA, location);
                        startActivity(i);
                    }
                });

    }

    @Override
    protected void onStop() {
        stopFusedLocation();
        super.onStop();
    }


    // check if google play services is installed on the device
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(),
                        "This device is supported. Please download google play services", Toast.LENGTH_LONG)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }


    public void startFusedLocation() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnectionSuspended(int cause) {
                        }

                        @Override
                        public void onConnected(Bundle connectionHint) {

                        }
                    }).addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {

                        @Override
                        public void onConnectionFailed(ConnectionResult result) {

                        }
                    }).build();
            mGoogleApiClient.connect();
        } else {
            mGoogleApiClient.connect();
        }
    }

    public void stopFusedLocation() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }


    public void registerRequestUpdate(final LocationListener listener) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000); // every second

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                try {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, listener);
                } catch (SecurityException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    if (!isGoogleApiClientConnected()) {
                        mGoogleApiClient.connect();
                    }
                    registerRequestUpdate(listener);
                }
            }
        }, 1000);
    }

    public boolean isGoogleApiClientConnected() {
        return mGoogleApiClient != null && mGoogleApiClient.isConnected();
    }

    @Override
    public void onLocationChanged(Location location) {
        setFusedLatitude(location.getLatitude());
        setFusedLongitude(location.getLongitude());

//        Toast.makeText(getApplicationContext(), "NEW LOCATION RECEIVED", Toast.LENGTH_LONG).show();

//        latitude.setText(getString(R.string.latitude_string) +" "+ getFusedLatitude());
//        longitude.setText(getString(R.string.longitude_string) +" "+ getFusedLongitude());

        lat = getFusedLatitude();
        lon = getFusedLongitude();

//        startBackgroundTask();


        for (int i = 0; i < locationList.size(); i++) {
            float[] distance = new float[1];
            Location.distanceBetween(locationList.get(i).getLat(), locationList.get(i).getLon(), lat, lon, distance);
            // distance[0] is now the distance between these lat/lons in meters
            if (distance[0] < 10) {

                Utilz.showAlert(this, "Alert", "Your'e near " + i + " Location");

            }
        }


    }

    public double getFusedLatitude() {
        return fusedLatitude;
    }

    public void setFusedLatitude(double lat) {
        fusedLatitude = lat;
    }

    public double getFusedLongitude() {
        return fusedLongitude;
    }

    public void setFusedLongitude(double lon) {
        fusedLongitude = lon;
    }


}
