package com.ss.locationapp.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.ss.locationapp.Listeners;
import com.ss.locationapp.POJOS.Location;
import com.ss.locationapp.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by samar_000 on 11/4/2016.
 */

public class NearbyLocationAdapter extends RecyclerView.Adapter<NearbyLocationAdapter.NearbyLocationHolder> {


    private ArrayList<Location> data;
    private Listeners.LocationListListener listener;
    private Context context;


    public NearbyLocationAdapter(ArrayList<Location> data, Listeners.LocationListListener listener) {
        this.data = data;
        this.listener = listener;
    }

    public NearbyLocationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_location, parent, false);
        context = parent.getContext();
        return new NearbyLocationHolder(v);
    }

    @Override
    public void onBindViewHolder(NearbyLocationHolder holder, int position) {

        holder.locationName.setText(data.get(position).getTitle());


        Double a = Double.parseDouble(data.get(position).getDistance());
        holder.miles.setText(new DecimalFormat("##.##").format(a) + " miles");

        Picasso.with(context).load(data.get(position).getImg().get(0).getImage()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class NearbyLocationHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.list_location_image)
        ImageView imageView;

        @BindView(R.id.location_name)
        TextView locationName;

        @BindView(R.id.miles)
        TextView miles;

        public NearbyLocationHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(data.get(getLayoutPosition()));
                }
            });
        }


    }
}
