package com.ss.locationapp.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.ss.locationapp.Listeners;
import com.ss.locationapp.POJOS.Img;
import com.ss.locationapp.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by samar_000 on 11/14/2016.
 */

public class BottomFragmentAdapter extends RecyclerView.Adapter<BottomFragmentAdapter.BFViewHolder> {

    private Context context;
    private List<Img> data;
    private Listeners.ImageChangeListener listener;


    public BottomFragmentAdapter(List<Img> data, Listeners.ImageChangeListener listener) {
        this.data = data;
        this.listener = listener;
    }

    @Override
    public BFViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_bottom_pics, parent, false);
        this.context = parent.getContext();
        return new BFViewHolder(v);
    }

    @Override
    public void onBindViewHolder(BFViewHolder holder, int position) {
        Picasso.with(context).load(data.get(position).getImage()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class BFViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_image)
        ImageView imageView;

        public BFViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onImageClick(data.get(getLayoutPosition()).getImage());
                }
            });
        }
    }
}
