package com.ss.locationapp.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.ss.locationapp.R;

/**
 * Created by samar_000 on 11/18/2016.
 */

public class SplashScreen extends AppCompatActivity {

    private static int DURATION = 3000;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activiy_splash_screen);

        new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashScreen.this, AndroidLocationActivity.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, DURATION);

    }
}
