package com.ss.locationapp.ui.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;
import com.ss.locationapp.Listeners;
import com.ss.locationapp.POJOS.LocPOJO;
import com.ss.locationapp.POJOS.Location;
import com.ss.locationapp.R;
import com.ss.locationapp.Utilz;
import com.ss.locationapp.ui.fragment.BottomFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by samar_000 on 11/14/2016.
 */

public class DetailActivity extends BaseActivity implements Listeners.ImageChangeListener {


    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.desc)
    TextView tvDesc;
    @BindView(R.id.frameLayoutProgress)
    FrameLayout frameLayout;
    @BindView(R.id.progressbar)
    ProgressBar progressBar;
    @BindView(R.id.direction)
    FrameLayout frameLayout1;
    @BindView(R.id.playVideo)
    FrameLayout frameLayout2;
    @BindView(R.id.share)
    FrameLayout frameLayout3;
    private Location location;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);
        ButterKnife.bind(this);
        location = getIntent().getParcelableExtra(AndroidLocationActivity.DATA);
        init();
    }


    private void init() {

        setDrawable(frameLayout1);
        setDrawable(frameLayout2);
        setDrawable(frameLayout3);


        tvDesc.setText(location.getDes());

        getSupportActionBar().setTitle(location.getTitle());

        getToolbar().setNavigationIcon(R.drawable.ic_cross);
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if (!location.getVideo().isEmpty()) {
            //TODO play video

            frameLayout.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);

            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webView.loadUrl(location.getVideo());


            webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    progressBar.setVisibility(View.GONE);

                }

                @Override
                public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                    super.onReceivedError(view, request, error);
                    progressBar.setVisibility(View.GONE);

                }

                @Override
                public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                    super.onReceivedHttpError(view, request, errorResponse);
                    progressBar.setVisibility(View.GONE);

                }
            });


        } else {
            frameLayout.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            displayImage(location.getImg().get(0).getImage());
        }


        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        BottomFragment fragment = new BottomFragment();
        Bundle b = new Bundle();
        b.putParcelable(AndroidLocationActivity.DATA, location);
        fragment.setArguments(b);
        ft.replace(R.id.fragment_layout, fragment);
        ft.commit();

    }


    @OnClick(R.id.share)
    public void openShareIntent() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Tour App Text");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void setDrawable(FrameLayout fl) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fl.setBackground(getDrawable(R.drawable.ripple));
        }
    }

    @OnClick(R.id.direction)
    void onDirClick() {

        Intent i = new Intent(this, SimpleDirectionActivity.class);

        LocPOJO locPOJO = location.getLocPOJO();
        locPOJO.setDestination(new LatLng(Double.parseDouble(location.getLat()), Double.parseDouble(location.get_long())));
        location.setLocPOJO(locPOJO);

        i.putExtra(AndroidLocationActivity.DATA, location);
        startActivity(i);
    }

    @OnClick(R.id.playVideo)
    void onPlayClick() {
        if (!location.getVideo().isEmpty()) {
            //TODO play video
            frameLayout.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webView.loadUrl(location.getVideo());
        }
    }


    @OnClick(R.id.desc)
    void OnDescClick() {
        Utilz.showAlert(this, "Description", location.getDes());
    }

    private void displayImage(String Url) {
        frameLayout.setVisibility(View.GONE);
        webView.reload();
        imageView.setVisibility(View.VISIBLE);
        imageView.setImageResource(0);
        Picasso.with(this).load(Url).into(imageView);
    }

    @Override
    public void onImageClick(String url) {
        displayImage(url);
    }
}
