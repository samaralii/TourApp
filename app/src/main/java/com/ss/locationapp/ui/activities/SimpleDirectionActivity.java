package com.ss.locationapp.ui.activities;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.ss.locationapp.POJOS.LocPOJO;
import com.ss.locationapp.POJOS.Location;
import com.ss.locationapp.POJOS.locationapipojo.LocationApiPOJO;
import com.ss.locationapp.POJOS.locationapipojo.Route;
import com.ss.locationapp.R;
import com.ss.locationapp.Utilz;
import com.ss.locationapp.WebServices;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SAMAR on 11/19/2016.
 */

public class SimpleDirectionActivity extends AppCompatActivity implements OnMapReadyCallback {
    @BindString(R.string.google_map_api_key)
    public String apiKey;
    //    private Button btnRequestDirection;
    private GoogleMap googleMap;
    private LatLng camera;
    private LatLng origin;
    private LatLng destination;
    private Location location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_direction);

//        btnRequestDirection = (Button) findViewById(R.id.btn_request_direction);
//        btnRequestDirection.setOnClickListener(this);
        location = getIntent().getParcelableExtra(AndroidLocationActivity.DATA);

        origin = new LatLng(location.getLocPOJO().getCurrent().latitude, location.getLocPOJO().getCurrent().longitude);
        destination = new LatLng(location.getLocPOJO().getDestination().latitude, location.getLocPOJO().getDestination().longitude);
        camera = new LatLng(location.getLocPOJO().getCurrent().latitude, location.getLocPOJO().getCurrent().longitude);

        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);

        LocPOJO pojo = new LocPOJO();
        pojo.setDestination(destination);
        pojo.setCurrent(origin);

        findRoutes(pojo);

    }

    private void _setDirection(Route route) {


        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(origin, 15));


//        tvKm.setText(route.getLegs().get(0).getDistance().getText());
//        tvMins.setText(route.getLegs().get(0).getDuration().getText());

        LatLng start = new LatLng(route.getLegs().get(0).getStartLocation().getLat(),
                route.getLegs().get(0).getStartLocation().getLng());

        LatLng end = new LatLng(route.getLegs().get(0).getEndLocation().getLat(),
                route.getLegs().get(0).getEndLocation().getLng());

        googleMap.addMarker(new MarkerOptions().position(start));
        googleMap.addMarker(new MarkerOptions().position(end));


        List<LatLng> polyz = decodePoly(route.getOverviewPolyline().getPoints());


        for (int i = 0; i < polyz.size(); i++) {

            LatLng src = polyz.get(i);
            LatLng dest = polyz.get(i + 1);

            Polyline line = googleMap.addPolyline(new PolylineOptions()
                    .add(new LatLng(src.latitude, src.longitude),
                            new LatLng(dest.latitude, dest.longitude))
                    .width(7.5f).color(Color.BLUE).geodesic(true));
        }

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(camera, 13));
    }


    public void findRoutes(LocPOJO locationPOJO) {

        WebServices api = Utilz.getRetrofit(Utilz.AppConstant.GOOGLE_MAP_URL).create(WebServices.class);

        String origin = locationPOJO.getCurrent().latitude + "," + locationPOJO.getCurrent().longitude;
        String destination = locationPOJO.getDestination().latitude + "," + locationPOJO.getDestination().longitude;


        api.getRoutes(origin, destination, apiKey).enqueue(new Callback<LocationApiPOJO>() {
            @Override
            public void onResponse(Call<LocationApiPOJO> call, Response<LocationApiPOJO> response) {

                if (response == null || !response.isSuccessful()) {
                    Utilz.tmsg(SimpleDirectionActivity.this, "Error");
                    return;
                }

                String text = response.body().getStatus();

                if (response.body().getRoutes().isEmpty()) {

                    Utilz.showAlert(SimpleDirectionActivity.this, "Error", "No Route Found", new Utilz.showAlertListener.showAlertOkListener() {
                        @Override
                        public void onOkClick(DialogInterface dialogInterface) {
                            dialogInterface.dismiss();
                            finish();
                        }
                    });


                } else {
                    _setDirection(response.body().getRoutes().get(0));
                }


            }

            @Override
            public void onFailure(Call<LocationApiPOJO> call, Throwable t) {
                Log.e("getRoutes", "onFailure: ", t);
                Utilz.tmsg(SimpleDirectionActivity.this, "Error");
            }
        });

    }


    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }


}
