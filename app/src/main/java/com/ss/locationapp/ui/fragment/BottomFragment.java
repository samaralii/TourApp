package com.ss.locationapp.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ss.locationapp.Listeners;
import com.ss.locationapp.POJOS.Location;
import com.ss.locationapp.R;
import com.ss.locationapp.ui.activities.AndroidLocationActivity;
import com.ss.locationapp.ui.adapters.BottomFragmentAdapter;
import com.ss.locationapp.ui.activities.DetailActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by samar_000 on 11/14/2016.
 */

public class BottomFragment extends BaseFragment {


    @BindView(R.id.imagesList)
    RecyclerView imagesList;

    private Unbinder unbinder;

    private Location location;

    private Listeners.ImageChangeListener listener;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        location = getArguments().getParcelable(AndroidLocationActivity.DATA);
        listener = (DetailActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_fragment, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {

        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        lm.setOrientation(LinearLayoutManager.HORIZONTAL);

        imagesList.setLayoutManager(lm);
        imagesList.hasFixedSize();

        BottomFragmentAdapter adapter = new BottomFragmentAdapter(location.getImg(), new Listeners.ImageChangeListener() {
            @Override
            public void onImageClick(String url) {
                listener.onImageClick(url);
            }
        });
        imagesList.setAdapter(adapter);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
