package com.ss.locationapp.ui.activities;

import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ss.locationapp.R;
import com.ss.locationapp.WebServices;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by samar_000 on 11/4/2016.
 */

public class BaseActivity extends AppCompatActivity {

    protected WebServices api;
    private Toolbar toolbar;


    void InitializeToolbar() {
        toolbar = (Toolbar) findViewById(R.id.app_toolbar);

        if (toolbar != null)
            setSupportActionBar(toolbar);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        InitializeToolbar();
    }

    public Retrofit getRetrofit(String url) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        return new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    protected Toolbar getToolbar() {
        return toolbar;
    }

}
