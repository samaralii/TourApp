package com.ss.locationapp.backgroudServices;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.ss.locationapp.POJOS.LocationPOJO;
import com.ss.locationapp.POJOS.POJO;
import com.ss.locationapp.Utilz;
import com.ss.locationapp.WebServices;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by samar_000 on 12/2/2016.
 */

public class BackgroundTask extends Service implements LocationListener {

    public static final String DATA = "data";
    public static final String BROADCAST_ACTION = " com.ss.locationapp.ui.activities";
    private static final String TAG = BackgroundTask.class.getName().toUpperCase();
    private static final String TAG_B = "BroadcastService";
    private final Handler handler = new Handler();
    List<POJO> locationList;
    private Intent intent;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double fusedLatitude = 0.0;
    private double fusedLongitude = 0.0;
    private double lat = 0.0;
    private double lon = 0.0;
    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            getNearbyPlaces();
            handler.postDelayed(this, 10000); // 5 seconds
        }
    };

    @Override
    public void onLocationChanged(Location location) {

        setFusedLatitude(location.getLatitude());
        setFusedLongitude(location.getLongitude());

//        Toast.makeText(getApplicationContext(), "NEW LOCATION RECEIVED", Toast.LENGTH_LONG).show();

//        latitude.setText(getString(R.string.latitude_string) +" "+ getFusedLatitude());
//        longitude.setText(getString(R.string.longitude_string) +" "+ getFusedLongitude());

        lat = getFusedLatitude();
        lon = getFusedLongitude();

//        startBackgroundTask();
//        getNearbyPlaces();

//
//        for (int i = 0; i < locationList.size(); i++) {
//            float[] distance = new float[1];
//            Location.distanceBetween(locationList.get(i).getLat(), locationList.get(i).getLon(), lat, lon, distance);
//            // distance[0] is now the distance between these lat/lons in meters
//            if (distance[0] < 10) {
//
////                Utilz.showAlert(this, "Alert", "Your'e near " + i + " Location");
//
//            }
//        }


    }

    public void startFusedLocation() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnectionSuspended(int cause) {
                        }

                        @Override
                        public void onConnected(Bundle connectionHint) {

                        }
                    }).addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {

                        @Override
                        public void onConnectionFailed(ConnectionResult result) {

                        }
                    }).build();
            mGoogleApiClient.connect();
        } else {
            mGoogleApiClient.connect();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        intent = new Intent(BROADCAST_ACTION);

        if (checkPlayServices()) {
            startFusedLocation();
            registerRequestUpdate(this);
        }


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        final String message = "hello";

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
////                Log.e(TAG, "run: " + message);
//                getNearbyPlaces();
//            }
//        }, 9000);

        handler.removeCallbacks(sendUpdatesToUI);
        handler.postDelayed(sendUpdatesToUI, 1000); // 1 second


        return START_STICKY;
    }

    private void DisplayLoggingInfo(String jsonResponse) {
        Log.d(TAG, "entered DisplayLoggingInfo");

        intent.putExtra("counter", "hi");
        sendBroadcast(intent);
    }

    private void getNearbyPlaces() {

        WebServices api = Utilz.getRetrofit(Utilz.AppConstant.BASE_URL).create(WebServices.class);


        api.getLocation().enqueue(new Callback<LocationPOJO>() {
            @Override
            public void onResponse(Call<LocationPOJO> call, Response<LocationPOJO> response) {

                if (response == null && !response.isSuccessful()) {
                    intent.putExtra("error", "ERROR");
                    sendBroadcast(intent);
                    return;
                }

                intent.putExtra("counter", response.body().getLocation());
                sendBroadcast(intent);

                for (int i = 0; i < response.body().getLocation().size(); i++) {
                    Log.d(TAG, "onResponse: " + response.body().getLocation().get(i).getTitle());
                }


            }

            @Override
            public void onFailure(Call<LocationPOJO> call, Throwable t) {
                Log.e("GET LOCATION", "onFailure: ", t);
                intent.putExtra("error", "ERROR");
                sendBroadcast(intent);
            }
        });


    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
//                Toast.makeText(getApplicationContext(),
//                        "This device is supported. Please download google play services", Toast.LENGTH_LONG)
//                        .show();
            } else {
//                Toast.makeText(getApplicationContext(),
//                        "This device is not supported.", Toast.LENGTH_LONG)
//                        .show();
//                finish();
            }
            return false;
        }
        return true;
    }


    public void registerRequestUpdate(final LocationListener listener) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000); // every second

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                try {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, listener);
                } catch (SecurityException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    if (!isGoogleApiClientConnected()) {
                        mGoogleApiClient.connect();
                    }
                    registerRequestUpdate(listener);
                }
            }
        }, 1000);
    }

    public boolean isGoogleApiClientConnected() {
        return mGoogleApiClient != null && mGoogleApiClient.isConnected();
    }

    public double getFusedLatitude() {
        return fusedLatitude;
    }

    public void setFusedLatitude(double lat) {
        fusedLatitude = lat;
    }

    public double getFusedLongitude() {
        return fusedLongitude;
    }

    public void setFusedLongitude(double lon) {
        fusedLongitude = lon;
    }

    public void stopFusedLocation() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(sendUpdatesToUI);
        super.onDestroy();
        stopFusedLocation();

    }

}
