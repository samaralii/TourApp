package com.ss.locationapp.POJOS;

/**
 * Created by samar_000 on 11/2/2016.
 */

public class POJO {

    double lat;
    double lon;

    public POJO() {

    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

}
