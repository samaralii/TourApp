
package com.ss.locationapp.POJOS;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Location implements Parcelable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("des")
    @Expose
    private String des;
    @SerializedName("img")
    @Expose
    private List<Img> img = new ArrayList<Img>();
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("long")
    @Expose
    private String _long;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("video")
    @Expose
    private String video;

    private LocPOJO locPOJO;


    public Location() {

    }

    protected Location(Parcel in) {
        id = in.readString();
        title = in.readString();
        des = in.readString();
        img = in.createTypedArrayList(Img.CREATOR);
        lat = in.readString();
        _long = in.readString();
        distance = in.readString();
        video = in.readString();
        locPOJO = in.readParcelable(LocPOJO.class.getClassLoader());
    }

    public static final Creator<Location> CREATOR = new Creator<Location>() {
        @Override
        public Location createFromParcel(Parcel in) {
            return new Location(in);
        }

        @Override
        public Location[] newArray(int size) {
            return new Location[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public List<Img> getImg() {
        return img;
    }

    public void setImg(List<Img> img) {
        this.img = img;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String get_long() {
        return _long;
    }

    public void set_long(String _long) {
        this._long = _long;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public LocPOJO getLocPOJO() {
        return locPOJO;
    }

    public void setLocPOJO(LocPOJO locPOJO) {
        this.locPOJO = locPOJO;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(des);
        parcel.writeTypedList(img);
        parcel.writeString(lat);
        parcel.writeString(_long);
        parcel.writeString(distance);
        parcel.writeString(video);
        parcel.writeParcelable(locPOJO, i);
    }
}
