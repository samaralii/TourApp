package com.ss.locationapp.POJOS;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by samar_000 on 11/29/2016.
 */

public class LocPOJO implements Parcelable{
    private LatLng current;
    private LatLng destination;

    public LocPOJO() {

    }

    protected LocPOJO(Parcel in) {
        current = in.readParcelable(LatLng.class.getClassLoader());
        destination = in.readParcelable(LatLng.class.getClassLoader());
    }

    public static final Creator<LocPOJO> CREATOR = new Creator<LocPOJO>() {
        @Override
        public LocPOJO createFromParcel(Parcel in) {
            return new LocPOJO(in);
        }

        @Override
        public LocPOJO[] newArray(int size) {
            return new LocPOJO[size];
        }
    };

    public LatLng getCurrent() {
        return current;
    }

    public void setCurrent(LatLng current) {
        this.current = current;
    }

    public LatLng getDestination() {
        return destination;
    }

    public void setDestination(LatLng destination) {
        this.destination = destination;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(current, i);
        parcel.writeParcelable(destination, i);
    }
}
