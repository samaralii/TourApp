
package com.ss.locationapp.POJOS;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationPOJO implements Parcelable{

    @SerializedName("Location")
    @Expose
    private ArrayList<Location> location = new ArrayList<Location>();

    protected LocationPOJO(Parcel in) {
        location = in.createTypedArrayList(Location.CREATOR);
    }

    public static final Creator<LocationPOJO> CREATOR = new Creator<LocationPOJO>() {
        @Override
        public LocationPOJO createFromParcel(Parcel in) {
            return new LocationPOJO(in);
        }

        @Override
        public LocationPOJO[] newArray(int size) {
            return new LocationPOJO[size];
        }
    };

    /**
     * 
     * @return
     *     The location
     */
    public ArrayList<Location> getLocation() {
        return location;
    }

    /**
     * 
     * @param location
     *     The Location
     */
    public void setLocation(ArrayList<Location> location) {
        this.location = location;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(location);
    }
}
