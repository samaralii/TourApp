package com.ss.locationapp;


import com.ss.locationapp.POJOS.Location;

/**
 * Created by samar_000 on 11/4/2016.
 */

public class Listeners {

    public interface LocationListListener {
        void onClick(Location location);
    }

    public interface ImageChangeListener {
        void onImageClick(String url);
    }
}
