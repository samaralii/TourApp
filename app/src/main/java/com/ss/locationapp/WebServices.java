package com.ss.locationapp;

import com.ss.locationapp.POJOS.LocationPOJO;
import com.ss.locationapp.POJOS.locationapipojo.LocationApiPOJO;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;



/**
 * Created by samar_000 on 11/4/2016.
 */

public interface WebServices {


    @GET("locations/api_locations")
    Call<LocationPOJO> getLocation();


    @GET("json?")
    Call<LocationApiPOJO> getRoutes(@Query("origin") String origin,
                                    @Query("destination") String destination,
                                    @Query("key") String key
    );
}
