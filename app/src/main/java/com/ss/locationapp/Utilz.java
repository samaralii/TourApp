package com.ss.locationapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

/**
 * Created by samar_000 on 11/2/2016.
 */

public class Utilz {


    /*Show Alert*/
    public static void showAlert(Context context, String title, String message) {
        AlertDialog dialog = new AlertDialog.Builder(context).
                setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create();
        dialog.show();

    }

    /*Show Alert with okay and cancel button*/
    public static void showAlert(Context context, String title, String message,
                                 final showAlertListener.showAlertOkCancelListener listener) {
        AlertDialog dialog = new AlertDialog.Builder(context).
                setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onOkClick(dialogInterface);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onCancelClick(dialogInterface);
                    }
                })
                .create();
        dialog.show();

    }

    /*Show Alert with okay button*/
    public static void showAlert(Context context, String title, String message,
                                 final showAlertListener.showAlertOkListener listener) {
        AlertDialog dialog = new AlertDialog.Builder(context).
                setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onOkClick(dialogInterface);
                    }
                })
                .create();
        dialog.show();

    }

    /*Show Alert with cancel button*/
    public static void showAlert(Context context, String title, String message,
                                 final showAlertListener.showAlertCancelListener listener) {
        AlertDialog dialog = new AlertDialog.Builder(context).
                setTitle(title)
                .setMessage(message)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onCancelClick(dialogInterface);
                    }
                })
                .create();
        dialog.show();

    }

    public static void tmsg(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void tmsg(Context context, String message, int duration) {
        Toast.makeText(context, message, duration).show();
    }

    public static ProgressDialog getProgressDialog(Context context, String title, String message) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        return progressDialog;
    }

    public static ProgressDialog getProgressDialog(Context context) {
        return getProgressDialog(context, "Please Wait", "Loading...");
    }

    public static class showAlertListener {

        interface showAlertOkCancelListener {
            void onOkClick(DialogInterface dialogInterface);

            void onCancelClick(DialogInterface dialogInterface);
        }

        public interface showAlertOkListener {
            void onOkClick(DialogInterface dialogInterface);
        }


        interface showAlertCancelListener {
            void onCancelClick(DialogInterface dialogInterface);
        }
    }

    public class AppConstant {
        public final static String BASE_URL = "http://dev.logisticslogic.com/projects/tourapp/web/";
        public final static String GOOGLE_MAP_URL = "https://maps.googleapis.com/maps/api/directions/";

    }

}
